# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from . from . from . from . from . from . from . from . from . from . from . from . from . from . from . from . from . import user_pb2 as user__pb2


class UserStub(object):
  """string invite_method = 11; // website or telegram
  string date = 12; // declared in engine
  int32 refracto_user_id = 13; // declared in engine
  int32 invited_by = 10; // referral code, store in cookies
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.UserCreate = channel.unary_unary(
        '/user.User/UserCreate',
        request_serializer=user__pb2.CreateUser.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )
    self.UserAll = channel.unary_unary(
        '/user.User/UserAll',
        request_serializer=user__pb2.IDs.SerializeToString,
        response_deserializer=user__pb2.UserAllInfo.FromString,
        )
    self.UserProfile = channel.unary_unary(
        '/user.User/UserProfile',
        request_serializer=user__pb2.IDs.SerializeToString,
        response_deserializer=user__pb2.UserProfileInfo.FromString,
        )
    self.UserFinancial = channel.unary_unary(
        '/user.User/UserFinancial',
        request_serializer=user__pb2.IDs.SerializeToString,
        response_deserializer=user__pb2.UserFinancialInfo.FromString,
        )
    self.UserAdvanced = channel.unary_unary(
        '/user.User/UserAdvanced',
        request_serializer=user__pb2.IDs.SerializeToString,
        response_deserializer=user__pb2.UserAdvancedInfo.FromString,
        )
    self.UserEditName = channel.unary_unary(
        '/user.User/UserEditName',
        request_serializer=user__pb2.EditName.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )
    self.UserEditSurname = channel.unary_unary(
        '/user.User/UserEditSurname',
        request_serializer=user__pb2.EditSurname.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )
    self.UserEditRefractoUsername = channel.unary_unary(
        '/user.User/UserEditRefractoUsername',
        request_serializer=user__pb2.EditRefractoUsername.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )
    self.UserEditLanguage = channel.unary_unary(
        '/user.User/UserEditLanguage',
        request_serializer=user__pb2.EditLanguage.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )
    self.UserEditPassword = channel.unary_unary(
        '/user.User/UserEditPassword',
        request_serializer=user__pb2.EditPassword.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )
    self.UserEditEmail = channel.unary_unary(
        '/user.User/UserEditEmail',
        request_serializer=user__pb2.EditEmail.SerializeToString,
        response_deserializer=user__pb2.Status.FromString,
        )


class UserServicer(object):
  """string invite_method = 11; // website or telegram
  string date = 12; // declared in engine
  int32 refracto_user_id = 13; // declared in engine
  int32 invited_by = 10; // referral code, store in cookies
  """

  def UserCreate(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserAll(self, request, context):
    """uses refracto_user_id, auth credentials (token/session)
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserProfile(self, request, context):
    """uses refracto_user_id, auth credentials (token/session)
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserFinancial(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserAdvanced(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserEditName(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserEditSurname(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserEditRefractoUsername(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserEditLanguage(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserEditPassword(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UserEditEmail(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_UserServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'UserCreate': grpc.unary_unary_rpc_method_handler(
          servicer.UserCreate,
          request_deserializer=user__pb2.CreateUser.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
      'UserAll': grpc.unary_unary_rpc_method_handler(
          servicer.UserAll,
          request_deserializer=user__pb2.IDs.FromString,
          response_serializer=user__pb2.UserAllInfo.SerializeToString,
      ),
      'UserProfile': grpc.unary_unary_rpc_method_handler(
          servicer.UserProfile,
          request_deserializer=user__pb2.IDs.FromString,
          response_serializer=user__pb2.UserProfileInfo.SerializeToString,
      ),
      'UserFinancial': grpc.unary_unary_rpc_method_handler(
          servicer.UserFinancial,
          request_deserializer=user__pb2.IDs.FromString,
          response_serializer=user__pb2.UserFinancialInfo.SerializeToString,
      ),
      'UserAdvanced': grpc.unary_unary_rpc_method_handler(
          servicer.UserAdvanced,
          request_deserializer=user__pb2.IDs.FromString,
          response_serializer=user__pb2.UserAdvancedInfo.SerializeToString,
      ),
      'UserEditName': grpc.unary_unary_rpc_method_handler(
          servicer.UserEditName,
          request_deserializer=user__pb2.EditName.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
      'UserEditSurname': grpc.unary_unary_rpc_method_handler(
          servicer.UserEditSurname,
          request_deserializer=user__pb2.EditSurname.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
      'UserEditRefractoUsername': grpc.unary_unary_rpc_method_handler(
          servicer.UserEditRefractoUsername,
          request_deserializer=user__pb2.EditRefractoUsername.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
      'UserEditLanguage': grpc.unary_unary_rpc_method_handler(
          servicer.UserEditLanguage,
          request_deserializer=user__pb2.EditLanguage.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
      'UserEditPassword': grpc.unary_unary_rpc_method_handler(
          servicer.UserEditPassword,
          request_deserializer=user__pb2.EditPassword.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
      'UserEditEmail': grpc.unary_unary_rpc_method_handler(
          servicer.UserEditEmail,
          request_deserializer=user__pb2.EditEmail.FromString,
          response_serializer=user__pb2.Status.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'user.User', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
