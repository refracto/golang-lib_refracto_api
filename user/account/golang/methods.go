package user

import (
	auth_pb "bitbucket.org/refracto/golang-lib_refracto_api/user/auth/golang"
	"context"
	"google.golang.org/grpc"
	"log"
	"time"
)

func (d *IDs) CheckSession(UserAuthServer string) bool {
	// Set up a connection to the server.
	conn, err := grpc.Dial(UserAuthServer, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := auth_pb.NewAuthenticationClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.CheckSession(ctx, &auth_pb.IDs{
		RefractoUserId: d.RefractoUserId,
		Auth: &auth_pb.Auth{
			Session: d.Auth.Session,
		},
	})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r.GetStatus()
}

func (d *IDs) CheckToken(UserAuthServer string) bool {
	// Set up a connection to the server.
	conn, err := grpc.Dial(UserAuthServer, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := auth_pb.NewAuthenticationClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.CheckToken(ctx, &auth_pb.IDs{
		RefractoUserId: d.RefractoUserId,
		Auth: &auth_pb.Auth{
			Token: d.Auth.Token,
		},
	})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r.GetStatus()
}

func (ids IDs) AuthChecker(UserAuthServer string) bool {
	// Set up a connection to the server.
	conn, err := grpc.Dial(UserAuthServer, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := auth_pb.NewAuthenticationClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	if ids.Auth.Token != "" {
		r, err := c.CheckToken(ctx, &auth_pb.IDs{
			RefractoUserId: ids.RefractoUserId,
			Auth: &auth_pb.Auth{
				Token: ids.Auth.Token,
			},
		})
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		return r.GetStatus()
	} else if ids.Auth.Session != "" {
		r, err := c.CheckSession(ctx, &auth_pb.IDs{
			RefractoUserId: ids.RefractoUserId,
			Auth: &auth_pb.Auth{
				Session: ids.Auth.Session,
			},
		})
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		return r.GetStatus()
	}
	return false
}
