#!/usr/bin/env bash

PACKAGE_PATH='user/invoice'
PROTO_NAME='invoice.proto'
PACKAGE_NAME='invoice'

echo "golang with grpc-gateway"
protoc -I/usr/local/include -I ${PACKAGE_PATH}/proto -I$GOPATH/src -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis  --go_out=plugins=grpc:${PACKAGE_PATH}/golang ./${PACKAGE_PATH}/proto/${PROTO_NAME}
echo "grpc-gateway"
protoc -I/usr/local/include -I ${PACKAGE_PATH}/proto -I$GOPATH/src   -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis   --grpc-gateway_out=logtostderr=true:${PACKAGE_PATH}/golang   ./${PACKAGE_PATH}/proto/${PROTO_NAME}
echo "swagger-gen"
protoc -I/usr/local/include -I ${PACKAGE_PATH}/proto   -I$GOPATH/src   -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis   --swagger_out=logtostderr=true:${PACKAGE_PATH}/proto  ./${PACKAGE_PATH}/proto/${PROTO_NAME}
echo "skip the tag for the generated XXX_* fields"
protoc-go-inject-tag  -input=./${PACKAGE_PATH}/golang/${PACKAGE_NAME}.pb.go -XXX_skip=gorm

echo "python with grpc-gateway"
python3 -m grpc_tools.protoc -I ${PACKAGE_PATH}/proto --python_out=./${PACKAGE_PATH}/python -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --grpc_python_out=./${PACKAGE_PATH}/python ${PACKAGE_PATH}/proto/${PROTO_NAME}

echo "replacing import to from . import"
grep -rl "${PACKAGE_NAME}_pb2" ${PACKAGE_PATH}/python | xargs sed -i '' -e "s/import ${PACKAGE_NAME}_pb2/from . import ${PACKAGE_NAME}_pb2/g"

echo "copying to python lib"
\cp -r "./${PACKAGE_PATH}/python/${PACKAGE_NAME}_pb2.py" "../python-lib_refracto_api/refracto_grpc/${PACKAGE_PATH}/${PACKAGE_NAME}_pb2.py"
\cp -r "./${PACKAGE_PATH}/python/${PACKAGE_NAME}_pb2_grpc.py" "../python-lib_refracto_api/refracto_grpc/${PACKAGE_PATH}/${PACKAGE_NAME}_pb2_grpc.py"

echo "done"