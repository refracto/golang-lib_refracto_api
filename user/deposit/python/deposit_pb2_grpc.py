# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from . import deposit_pb2 as deposit__pb2


class CreditCardStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.CreditCardList = channel.unary_unary(
                '/deposit.CreditCard/CreditCardList',
                request_serializer=deposit__pb2.IDs.SerializeToString,
                response_deserializer=deposit__pb2.CreditCardInfoList.FromString,
                )
        self.CreditCardClientToken = channel.unary_unary(
                '/deposit.CreditCard/CreditCardClientToken',
                request_serializer=deposit__pb2.IDs.SerializeToString,
                response_deserializer=deposit__pb2.CreditCardClientTokenResponse.FromString,
                )
        self.CreditCardPublicToken = channel.unary_unary(
                '/deposit.CreditCard/CreditCardPublicToken',
                request_serializer=deposit__pb2.IDs.SerializeToString,
                response_deserializer=deposit__pb2.CreditCardPublicTokenResponse.FromString,
                )
        self.CreditCardAdd = channel.unary_unary(
                '/deposit.CreditCard/CreditCardAdd',
                request_serializer=deposit__pb2.CreditCardAddRequest.SerializeToString,
                response_deserializer=deposit__pb2.Status.FromString,
                )
        self.CreditCardCharge = channel.unary_unary(
                '/deposit.CreditCard/CreditCardCharge',
                request_serializer=deposit__pb2.CreditCardChargeRequest.SerializeToString,
                response_deserializer=deposit__pb2.Status.FromString,
                )


class CreditCardServicer(object):
    """Missing associated documentation comment in .proto file."""

    def CreditCardList(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreditCardClientToken(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreditCardPublicToken(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreditCardAdd(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreditCardCharge(self, request, context):
        """rpc CreditCardDelete(PaymentMethodDeleteRequest) returns (Status) {
        option (google.api.http) = {
        post : "/deposit/CreditCard/Delete"
        body : "*"
        };
        }
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_CreditCardServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'CreditCardList': grpc.unary_unary_rpc_method_handler(
                    servicer.CreditCardList,
                    request_deserializer=deposit__pb2.IDs.FromString,
                    response_serializer=deposit__pb2.CreditCardInfoList.SerializeToString,
            ),
            'CreditCardClientToken': grpc.unary_unary_rpc_method_handler(
                    servicer.CreditCardClientToken,
                    request_deserializer=deposit__pb2.IDs.FromString,
                    response_serializer=deposit__pb2.CreditCardClientTokenResponse.SerializeToString,
            ),
            'CreditCardPublicToken': grpc.unary_unary_rpc_method_handler(
                    servicer.CreditCardPublicToken,
                    request_deserializer=deposit__pb2.IDs.FromString,
                    response_serializer=deposit__pb2.CreditCardPublicTokenResponse.SerializeToString,
            ),
            'CreditCardAdd': grpc.unary_unary_rpc_method_handler(
                    servicer.CreditCardAdd,
                    request_deserializer=deposit__pb2.CreditCardAddRequest.FromString,
                    response_serializer=deposit__pb2.Status.SerializeToString,
            ),
            'CreditCardCharge': grpc.unary_unary_rpc_method_handler(
                    servicer.CreditCardCharge,
                    request_deserializer=deposit__pb2.CreditCardChargeRequest.FromString,
                    response_serializer=deposit__pb2.Status.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'deposit.CreditCard', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class CreditCard(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def CreditCardList(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.CreditCard/CreditCardList',
            deposit__pb2.IDs.SerializeToString,
            deposit__pb2.CreditCardInfoList.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreditCardClientToken(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.CreditCard/CreditCardClientToken',
            deposit__pb2.IDs.SerializeToString,
            deposit__pb2.CreditCardClientTokenResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreditCardPublicToken(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.CreditCard/CreditCardPublicToken',
            deposit__pb2.IDs.SerializeToString,
            deposit__pb2.CreditCardPublicTokenResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreditCardAdd(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.CreditCard/CreditCardAdd',
            deposit__pb2.CreditCardAddRequest.SerializeToString,
            deposit__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreditCardCharge(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.CreditCard/CreditCardCharge',
            deposit__pb2.CreditCardChargeRequest.SerializeToString,
            deposit__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)


class BillingInfoStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.BillingInfoPaymentAll = channel.unary_unary(
                '/deposit.BillingInfo/BillingInfoPaymentAll',
                request_serializer=deposit__pb2.IDs.SerializeToString,
                response_deserializer=deposit__pb2.PaymentInfoList.FromString,
                )
        self.BillingInfoAccountBalance = channel.unary_unary(
                '/deposit.BillingInfo/BillingInfoAccountBalance',
                request_serializer=deposit__pb2.IDs.SerializeToString,
                response_deserializer=deposit__pb2.AccountBalanceInfo.FromString,
                )
        self.BillingInfoAccountBalanceCreditUpdate = channel.unary_unary(
                '/deposit.BillingInfo/BillingInfoAccountBalanceCreditUpdate',
                request_serializer=deposit__pb2.BillingInfoAccountBalanceCreditUpdateRequest.SerializeToString,
                response_deserializer=deposit__pb2.Status.FromString,
                )
        self.BillingInfoAccountBalanceCreditPay = channel.unary_unary(
                '/deposit.BillingInfo/BillingInfoAccountBalanceCreditPay',
                request_serializer=deposit__pb2.IDs.SerializeToString,
                response_deserializer=deposit__pb2.Status.FromString,
                )


class BillingInfoServicer(object):
    """Missing associated documentation comment in .proto file."""

    def BillingInfoPaymentAll(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def BillingInfoAccountBalance(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def BillingInfoAccountBalanceCreditUpdate(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def BillingInfoAccountBalanceCreditPay(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_BillingInfoServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'BillingInfoPaymentAll': grpc.unary_unary_rpc_method_handler(
                    servicer.BillingInfoPaymentAll,
                    request_deserializer=deposit__pb2.IDs.FromString,
                    response_serializer=deposit__pb2.PaymentInfoList.SerializeToString,
            ),
            'BillingInfoAccountBalance': grpc.unary_unary_rpc_method_handler(
                    servicer.BillingInfoAccountBalance,
                    request_deserializer=deposit__pb2.IDs.FromString,
                    response_serializer=deposit__pb2.AccountBalanceInfo.SerializeToString,
            ),
            'BillingInfoAccountBalanceCreditUpdate': grpc.unary_unary_rpc_method_handler(
                    servicer.BillingInfoAccountBalanceCreditUpdate,
                    request_deserializer=deposit__pb2.BillingInfoAccountBalanceCreditUpdateRequest.FromString,
                    response_serializer=deposit__pb2.Status.SerializeToString,
            ),
            'BillingInfoAccountBalanceCreditPay': grpc.unary_unary_rpc_method_handler(
                    servicer.BillingInfoAccountBalanceCreditPay,
                    request_deserializer=deposit__pb2.IDs.FromString,
                    response_serializer=deposit__pb2.Status.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'deposit.BillingInfo', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class BillingInfo(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def BillingInfoPaymentAll(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.BillingInfo/BillingInfoPaymentAll',
            deposit__pb2.IDs.SerializeToString,
            deposit__pb2.PaymentInfoList.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def BillingInfoAccountBalance(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.BillingInfo/BillingInfoAccountBalance',
            deposit__pb2.IDs.SerializeToString,
            deposit__pb2.AccountBalanceInfo.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def BillingInfoAccountBalanceCreditUpdate(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.BillingInfo/BillingInfoAccountBalanceCreditUpdate',
            deposit__pb2.BillingInfoAccountBalanceCreditUpdateRequest.SerializeToString,
            deposit__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def BillingInfoAccountBalanceCreditPay(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/deposit.BillingInfo/BillingInfoAccountBalanceCreditPay',
            deposit__pb2.IDs.SerializeToString,
            deposit__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
