package addressing

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
)

// ---------- Platform Stack ----------

func PlatformRay_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewPlatformStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Ray(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func PlatformDatabases_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewPlatformStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Databases(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func PlatformPayments_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewPlatformStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Payments(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func PlatformRepository_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewPlatformStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Repository(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

// ---------- User Stack ----------

func UserAuth_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewUserStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Auth(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func UserAccount_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewUserStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Account(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func UserDeposit_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewUserStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Deposit(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func UserNotificator_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewUserStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Notificator(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func UserInvoice_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewUserStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Invoice(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

// ---------- Data Storage Stack ----------

func DataSrorageInternalPostgres_server(addressing_server string) *ServerAddress {
	if addressing_server == "" {
		var serverAddress = &ServerAddress{}
		return serverAddress
	}
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewDataStorageStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.InternalPostgres(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func DataSrorageMysqlService_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewDataStorageStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.MysqlService(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}

func DataSroragePostgresService_server(addressing_server string) *ServerAddress {
	// Set up a connection to the server.
	conn, err := grpc.Dial(addressing_server, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewDataStorageStackClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.PostgresService(ctx, &Request{Requested: true})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	return r
}
