// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.10.0
// source: addressing.proto

// protoc -I network/addressing/proto --go_out=plugins=grpc:network/addressing/golang network/addressing/proto/addressing.proto
// python -m grpc_tools.protoc -I network/addressing/proto --python_out=network/addressing/python --grpc_python_out=network/addressing/python network/addressing/proto/addressing.proto
// python -m grpc_tools.protoc -I network/addressing/proto --python_out=./platform/payments/python --grpc_python_out=./platform/payments/python platform/payments/proto/payments.proto

package addressing

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type Request struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Requested bool `protobuf:"varint,1,opt,name=requested,proto3" json:"requested,omitempty"`
}

func (x *Request) Reset() {
	*x = Request{}
	if protoimpl.UnsafeEnabled {
		mi := &file_addressing_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Request) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Request) ProtoMessage() {}

func (x *Request) ProtoReflect() protoreflect.Message {
	mi := &file_addressing_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Request.ProtoReflect.Descriptor instead.
func (*Request) Descriptor() ([]byte, []int) {
	return file_addressing_proto_rawDescGZIP(), []int{0}
}

func (x *Request) GetRequested() bool {
	if x != nil {
		return x.Requested
	}
	return false
}

type ServerAddress struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Address string `protobuf:"bytes,1,opt,name=address,proto3" json:"address,omitempty"`
	Port    int32  `protobuf:"varint,2,opt,name=port,proto3" json:"port,omitempty"`
	Up      bool   `protobuf:"varint,3,opt,name=up,proto3" json:"up,omitempty"`
}

func (x *ServerAddress) Reset() {
	*x = ServerAddress{}
	if protoimpl.UnsafeEnabled {
		mi := &file_addressing_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ServerAddress) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ServerAddress) ProtoMessage() {}

func (x *ServerAddress) ProtoReflect() protoreflect.Message {
	mi := &file_addressing_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ServerAddress.ProtoReflect.Descriptor instead.
func (*ServerAddress) Descriptor() ([]byte, []int) {
	return file_addressing_proto_rawDescGZIP(), []int{1}
}

func (x *ServerAddress) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *ServerAddress) GetPort() int32 {
	if x != nil {
		return x.Port
	}
	return 0
}

func (x *ServerAddress) GetUp() bool {
	if x != nil {
		return x.Up
	}
	return false
}

var File_addressing_proto protoreflect.FileDescriptor

var file_addressing_proto_rawDesc = []byte{
	0x0a, 0x10, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x0a, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x22, 0x27,
	0x0a, 0x07, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x72, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x65, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x09, 0x72, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x65, 0x64, 0x22, 0x4d, 0x0a, 0x0d, 0x53, 0x65, 0x72, 0x76, 0x65,
	0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x6f, 0x72, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x04, 0x70, 0x6f, 0x72, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x75, 0x70, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x08, 0x52, 0x02, 0x75, 0x70, 0x32, 0xbd, 0x02, 0x0a, 0x09, 0x55, 0x73, 0x65, 0x72, 0x53,
	0x74, 0x61, 0x63, 0x6b, 0x12, 0x3b, 0x0a, 0x07, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12,
	0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e,
	0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22,
	0x00, 0x12, 0x3b, 0x0a, 0x07, 0x44, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x12, 0x13, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53,
	0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x12, 0x38,
	0x0a, 0x04, 0x41, 0x75, 0x74, 0x68, 0x12, 0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x12, 0x3f, 0x0a, 0x0b, 0x4e, 0x6f, 0x74, 0x69,
	0x66, 0x69, 0x63, 0x61, 0x74, 0x6f, 0x72, 0x12, 0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x12, 0x3b, 0x0a, 0x07, 0x49, 0x6e, 0x76,
	0x6f, 0x69, 0x63, 0x65, 0x12, 0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e,
	0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64,
	0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x32, 0x85, 0x02, 0x0a, 0x0d, 0x50, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x53, 0x74, 0x61, 0x63, 0x6b, 0x12, 0x37, 0x0a, 0x03, 0x52, 0x61, 0x79, 0x12,
	0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e,
	0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22,
	0x00, 0x12, 0x3d, 0x0a, 0x09, 0x44, 0x61, 0x74, 0x61, 0x62, 0x61, 0x73, 0x65, 0x73, 0x12, 0x13,
	0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67,
	0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00,
	0x12, 0x3c, 0x0a, 0x08, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x12, 0x13, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53,
	0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x12, 0x3e,
	0x0a, 0x0a, 0x52, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f, 0x72, 0x79, 0x12, 0x13, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53,
	0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x32, 0xa2,
	0x02, 0x0a, 0x10, 0x44, 0x61, 0x74, 0x61, 0x53, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65, 0x53, 0x74,
	0x61, 0x63, 0x6b, 0x12, 0x44, 0x0a, 0x10, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x50,
	0x6f, 0x73, 0x74, 0x67, 0x72, 0x65, 0x73, 0x12, 0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x12, 0x41, 0x0a, 0x0d, 0x49, 0x6e, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x6c, 0x52, 0x65, 0x64, 0x69, 0x73, 0x12, 0x13, 0x2e, 0x61, 0x64, 0x64,
	0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x53, 0x65, 0x72,
	0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x00, 0x12, 0x43, 0x0a, 0x0f,
	0x50, 0x6f, 0x73, 0x74, 0x67, 0x72, 0x65, 0x73, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12,
	0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e,
	0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22,
	0x00, 0x12, 0x40, 0x0a, 0x0c, 0x4d, 0x79, 0x73, 0x71, 0x6c, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x12, 0x13, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x2e, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x69, 0x6e, 0x67, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x22, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_addressing_proto_rawDescOnce sync.Once
	file_addressing_proto_rawDescData = file_addressing_proto_rawDesc
)

func file_addressing_proto_rawDescGZIP() []byte {
	file_addressing_proto_rawDescOnce.Do(func() {
		file_addressing_proto_rawDescData = protoimpl.X.CompressGZIP(file_addressing_proto_rawDescData)
	})
	return file_addressing_proto_rawDescData
}

var file_addressing_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_addressing_proto_goTypes = []interface{}{
	(*Request)(nil),       // 0: addressing.Request
	(*ServerAddress)(nil), // 1: addressing.ServerAddress
}
var file_addressing_proto_depIdxs = []int32{
	0,  // 0: addressing.UserStack.Account:input_type -> addressing.Request
	0,  // 1: addressing.UserStack.Deposit:input_type -> addressing.Request
	0,  // 2: addressing.UserStack.Auth:input_type -> addressing.Request
	0,  // 3: addressing.UserStack.Notificator:input_type -> addressing.Request
	0,  // 4: addressing.UserStack.Invoice:input_type -> addressing.Request
	0,  // 5: addressing.PlatformStack.Ray:input_type -> addressing.Request
	0,  // 6: addressing.PlatformStack.Databases:input_type -> addressing.Request
	0,  // 7: addressing.PlatformStack.Payments:input_type -> addressing.Request
	0,  // 8: addressing.PlatformStack.Repository:input_type -> addressing.Request
	0,  // 9: addressing.DataStorageStack.InternalPostgres:input_type -> addressing.Request
	0,  // 10: addressing.DataStorageStack.InternalRedis:input_type -> addressing.Request
	0,  // 11: addressing.DataStorageStack.PostgresService:input_type -> addressing.Request
	0,  // 12: addressing.DataStorageStack.MysqlService:input_type -> addressing.Request
	1,  // 13: addressing.UserStack.Account:output_type -> addressing.ServerAddress
	1,  // 14: addressing.UserStack.Deposit:output_type -> addressing.ServerAddress
	1,  // 15: addressing.UserStack.Auth:output_type -> addressing.ServerAddress
	1,  // 16: addressing.UserStack.Notificator:output_type -> addressing.ServerAddress
	1,  // 17: addressing.UserStack.Invoice:output_type -> addressing.ServerAddress
	1,  // 18: addressing.PlatformStack.Ray:output_type -> addressing.ServerAddress
	1,  // 19: addressing.PlatformStack.Databases:output_type -> addressing.ServerAddress
	1,  // 20: addressing.PlatformStack.Payments:output_type -> addressing.ServerAddress
	1,  // 21: addressing.PlatformStack.Repository:output_type -> addressing.ServerAddress
	1,  // 22: addressing.DataStorageStack.InternalPostgres:output_type -> addressing.ServerAddress
	1,  // 23: addressing.DataStorageStack.InternalRedis:output_type -> addressing.ServerAddress
	1,  // 24: addressing.DataStorageStack.PostgresService:output_type -> addressing.ServerAddress
	1,  // 25: addressing.DataStorageStack.MysqlService:output_type -> addressing.ServerAddress
	13, // [13:26] is the sub-list for method output_type
	0,  // [0:13] is the sub-list for method input_type
	0,  // [0:0] is the sub-list for extension type_name
	0,  // [0:0] is the sub-list for extension extendee
	0,  // [0:0] is the sub-list for field type_name
}

func init() { file_addressing_proto_init() }
func file_addressing_proto_init() {
	if File_addressing_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_addressing_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Request); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_addressing_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ServerAddress); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_addressing_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   3,
		},
		GoTypes:           file_addressing_proto_goTypes,
		DependencyIndexes: file_addressing_proto_depIdxs,
		MessageInfos:      file_addressing_proto_msgTypes,
	}.Build()
	File_addressing_proto = out.File
	file_addressing_proto_rawDesc = nil
	file_addressing_proto_goTypes = nil
	file_addressing_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// UserStackClient is the client API for UserStack service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type UserStackClient interface {
	Account(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Deposit(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Auth(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Notificator(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Invoice(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
}

type userStackClient struct {
	cc grpc.ClientConnInterface
}

func NewUserStackClient(cc grpc.ClientConnInterface) UserStackClient {
	return &userStackClient{cc}
}

func (c *userStackClient) Account(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.UserStack/Account", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userStackClient) Deposit(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.UserStack/Deposit", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userStackClient) Auth(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.UserStack/Auth", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userStackClient) Notificator(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.UserStack/Notificator", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userStackClient) Invoice(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.UserStack/Invoice", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserStackServer is the server API for UserStack service.
type UserStackServer interface {
	Account(context.Context, *Request) (*ServerAddress, error)
	Deposit(context.Context, *Request) (*ServerAddress, error)
	Auth(context.Context, *Request) (*ServerAddress, error)
	Notificator(context.Context, *Request) (*ServerAddress, error)
	Invoice(context.Context, *Request) (*ServerAddress, error)
}

// UnimplementedUserStackServer can be embedded to have forward compatible implementations.
type UnimplementedUserStackServer struct {
}

func (*UnimplementedUserStackServer) Account(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Account not implemented")
}
func (*UnimplementedUserStackServer) Deposit(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Deposit not implemented")
}
func (*UnimplementedUserStackServer) Auth(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Auth not implemented")
}
func (*UnimplementedUserStackServer) Notificator(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Notificator not implemented")
}
func (*UnimplementedUserStackServer) Invoice(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Invoice not implemented")
}

func RegisterUserStackServer(s *grpc.Server, srv UserStackServer) {
	s.RegisterService(&_UserStack_serviceDesc, srv)
}

func _UserStack_Account_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserStackServer).Account(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.UserStack/Account",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserStackServer).Account(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserStack_Deposit_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserStackServer).Deposit(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.UserStack/Deposit",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserStackServer).Deposit(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserStack_Auth_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserStackServer).Auth(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.UserStack/Auth",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserStackServer).Auth(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserStack_Notificator_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserStackServer).Notificator(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.UserStack/Notificator",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserStackServer).Notificator(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserStack_Invoice_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserStackServer).Invoice(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.UserStack/Invoice",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserStackServer).Invoice(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

var _UserStack_serviceDesc = grpc.ServiceDesc{
	ServiceName: "addressing.UserStack",
	HandlerType: (*UserStackServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Account",
			Handler:    _UserStack_Account_Handler,
		},
		{
			MethodName: "Deposit",
			Handler:    _UserStack_Deposit_Handler,
		},
		{
			MethodName: "Auth",
			Handler:    _UserStack_Auth_Handler,
		},
		{
			MethodName: "Notificator",
			Handler:    _UserStack_Notificator_Handler,
		},
		{
			MethodName: "Invoice",
			Handler:    _UserStack_Invoice_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "addressing.proto",
}

// PlatformStackClient is the client API for PlatformStack service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type PlatformStackClient interface {
	Ray(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Databases(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Payments(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	Repository(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
}

type platformStackClient struct {
	cc grpc.ClientConnInterface
}

func NewPlatformStackClient(cc grpc.ClientConnInterface) PlatformStackClient {
	return &platformStackClient{cc}
}

func (c *platformStackClient) Ray(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.PlatformStack/Ray", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *platformStackClient) Databases(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.PlatformStack/Databases", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *platformStackClient) Payments(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.PlatformStack/Payments", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *platformStackClient) Repository(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.PlatformStack/Repository", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PlatformStackServer is the server API for PlatformStack service.
type PlatformStackServer interface {
	Ray(context.Context, *Request) (*ServerAddress, error)
	Databases(context.Context, *Request) (*ServerAddress, error)
	Payments(context.Context, *Request) (*ServerAddress, error)
	Repository(context.Context, *Request) (*ServerAddress, error)
}

// UnimplementedPlatformStackServer can be embedded to have forward compatible implementations.
type UnimplementedPlatformStackServer struct {
}

func (*UnimplementedPlatformStackServer) Ray(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Ray not implemented")
}
func (*UnimplementedPlatformStackServer) Databases(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Databases not implemented")
}
func (*UnimplementedPlatformStackServer) Payments(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Payments not implemented")
}
func (*UnimplementedPlatformStackServer) Repository(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Repository not implemented")
}

func RegisterPlatformStackServer(s *grpc.Server, srv PlatformStackServer) {
	s.RegisterService(&_PlatformStack_serviceDesc, srv)
}

func _PlatformStack_Ray_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PlatformStackServer).Ray(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.PlatformStack/Ray",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PlatformStackServer).Ray(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _PlatformStack_Databases_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PlatformStackServer).Databases(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.PlatformStack/Databases",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PlatformStackServer).Databases(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _PlatformStack_Payments_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PlatformStackServer).Payments(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.PlatformStack/Payments",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PlatformStackServer).Payments(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _PlatformStack_Repository_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PlatformStackServer).Repository(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.PlatformStack/Repository",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PlatformStackServer).Repository(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

var _PlatformStack_serviceDesc = grpc.ServiceDesc{
	ServiceName: "addressing.PlatformStack",
	HandlerType: (*PlatformStackServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Ray",
			Handler:    _PlatformStack_Ray_Handler,
		},
		{
			MethodName: "Databases",
			Handler:    _PlatformStack_Databases_Handler,
		},
		{
			MethodName: "Payments",
			Handler:    _PlatformStack_Payments_Handler,
		},
		{
			MethodName: "Repository",
			Handler:    _PlatformStack_Repository_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "addressing.proto",
}

// DataStorageStackClient is the client API for DataStorageStack service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type DataStorageStackClient interface {
	InternalPostgres(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	InternalRedis(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	PostgresService(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
	MysqlService(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error)
}

type dataStorageStackClient struct {
	cc grpc.ClientConnInterface
}

func NewDataStorageStackClient(cc grpc.ClientConnInterface) DataStorageStackClient {
	return &dataStorageStackClient{cc}
}

func (c *dataStorageStackClient) InternalPostgres(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.DataStorageStack/InternalPostgres", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dataStorageStackClient) InternalRedis(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.DataStorageStack/InternalRedis", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dataStorageStackClient) PostgresService(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.DataStorageStack/PostgresService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dataStorageStackClient) MysqlService(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ServerAddress, error) {
	out := new(ServerAddress)
	err := c.cc.Invoke(ctx, "/addressing.DataStorageStack/MysqlService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DataStorageStackServer is the server API for DataStorageStack service.
type DataStorageStackServer interface {
	InternalPostgres(context.Context, *Request) (*ServerAddress, error)
	InternalRedis(context.Context, *Request) (*ServerAddress, error)
	PostgresService(context.Context, *Request) (*ServerAddress, error)
	MysqlService(context.Context, *Request) (*ServerAddress, error)
}

// UnimplementedDataStorageStackServer can be embedded to have forward compatible implementations.
type UnimplementedDataStorageStackServer struct {
}

func (*UnimplementedDataStorageStackServer) InternalPostgres(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InternalPostgres not implemented")
}
func (*UnimplementedDataStorageStackServer) InternalRedis(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InternalRedis not implemented")
}
func (*UnimplementedDataStorageStackServer) PostgresService(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PostgresService not implemented")
}
func (*UnimplementedDataStorageStackServer) MysqlService(context.Context, *Request) (*ServerAddress, error) {
	return nil, status.Errorf(codes.Unimplemented, "method MysqlService not implemented")
}

func RegisterDataStorageStackServer(s *grpc.Server, srv DataStorageStackServer) {
	s.RegisterService(&_DataStorageStack_serviceDesc, srv)
}

func _DataStorageStack_InternalPostgres_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DataStorageStackServer).InternalPostgres(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.DataStorageStack/InternalPostgres",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DataStorageStackServer).InternalPostgres(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _DataStorageStack_InternalRedis_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DataStorageStackServer).InternalRedis(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.DataStorageStack/InternalRedis",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DataStorageStackServer).InternalRedis(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _DataStorageStack_PostgresService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DataStorageStackServer).PostgresService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.DataStorageStack/PostgresService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DataStorageStackServer).PostgresService(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _DataStorageStack_MysqlService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DataStorageStackServer).MysqlService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/addressing.DataStorageStack/MysqlService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DataStorageStackServer).MysqlService(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

var _DataStorageStack_serviceDesc = grpc.ServiceDesc{
	ServiceName: "addressing.DataStorageStack",
	HandlerType: (*DataStorageStackServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "InternalPostgres",
			Handler:    _DataStorageStack_InternalPostgres_Handler,
		},
		{
			MethodName: "InternalRedis",
			Handler:    _DataStorageStack_InternalRedis_Handler,
		},
		{
			MethodName: "PostgresService",
			Handler:    _DataStorageStack_PostgresService_Handler,
		},
		{
			MethodName: "MysqlService",
			Handler:    _DataStorageStack_MysqlService_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "addressing.proto",
}
