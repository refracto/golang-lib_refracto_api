package addressing

import (
	"fmt"
)

func (addr *ServerAddress) Format() string {
	address := fmt.Sprintf("%s:%d", addr.Address, addr.Port)
	return address
}
