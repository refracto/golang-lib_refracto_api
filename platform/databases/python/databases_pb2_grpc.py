# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from . from . import databases_pb2 as databases__pb2


class DatabasesServiceStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.DatabasesGetOne = channel.unary_unary(
                '/databases.DatabasesService/DatabasesGetOne',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.DatabasesInfo.FromString,
                )
        self.DatabasesGetAll = channel.unary_unary(
                '/databases.DatabasesService/DatabasesGetAll',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.ListDatabasesInfo.FromString,
                )
        self.DatabasesAutopaymentOn = channel.unary_unary(
                '/databases.DatabasesService/DatabasesAutopaymentOn',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )
        self.DatabasesAutopaymentOff = channel.unary_unary(
                '/databases.DatabasesService/DatabasesAutopaymentOff',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )
        self.DatabasesNewLogin = channel.unary_unary(
                '/databases.DatabasesService/DatabasesNewLogin',
                request_serializer=databases__pb2.EditLogin.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )
        self.DatabasesNewPassword = channel.unary_unary(
                '/databases.DatabasesService/DatabasesNewPassword',
                request_serializer=databases__pb2.EditPassword.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )


class DatabasesServiceServicer(object):
    """Missing associated documentation comment in .proto file."""

    def DatabasesGetOne(self, request, context):
        """GetInfo
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DatabasesGetAll(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DatabasesAutopaymentOn(self, request, context):
        """Manage functions
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DatabasesAutopaymentOff(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DatabasesNewLogin(self, request, context):
        """EditFunctions
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DatabasesNewPassword(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_DatabasesServiceServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'DatabasesGetOne': grpc.unary_unary_rpc_method_handler(
                    servicer.DatabasesGetOne,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.DatabasesInfo.SerializeToString,
            ),
            'DatabasesGetAll': grpc.unary_unary_rpc_method_handler(
                    servicer.DatabasesGetAll,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.ListDatabasesInfo.SerializeToString,
            ),
            'DatabasesAutopaymentOn': grpc.unary_unary_rpc_method_handler(
                    servicer.DatabasesAutopaymentOn,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
            'DatabasesAutopaymentOff': grpc.unary_unary_rpc_method_handler(
                    servicer.DatabasesAutopaymentOff,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
            'DatabasesNewLogin': grpc.unary_unary_rpc_method_handler(
                    servicer.DatabasesNewLogin,
                    request_deserializer=databases__pb2.EditLogin.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
            'DatabasesNewPassword': grpc.unary_unary_rpc_method_handler(
                    servicer.DatabasesNewPassword,
                    request_deserializer=databases__pb2.EditPassword.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'databases.DatabasesService', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class DatabasesService(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def DatabasesGetOne(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.DatabasesService/DatabasesGetOne',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.DatabasesInfo.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DatabasesGetAll(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.DatabasesService/DatabasesGetAll',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.ListDatabasesInfo.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DatabasesAutopaymentOn(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.DatabasesService/DatabasesAutopaymentOn',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DatabasesAutopaymentOff(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.DatabasesService/DatabasesAutopaymentOff',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DatabasesNewLogin(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.DatabasesService/DatabasesNewLogin',
            databases__pb2.EditLogin.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DatabasesNewPassword(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.DatabasesService/DatabasesNewPassword',
            databases__pb2.EditPassword.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)


class UserDatabasesStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.UserDatabasesCreate = channel.unary_unary(
                '/databases.UserDatabases/UserDatabasesCreate',
                request_serializer=databases__pb2.CreateUserDatabases.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )
        self.UserDatabasesDelete = channel.unary_unary(
                '/databases.UserDatabases/UserDatabasesDelete',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )
        self.UserDatabasesGetOne = channel.unary_unary(
                '/databases.UserDatabases/UserDatabasesGetOne',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.UserDatabasesInfo.FromString,
                )
        self.UserDatabasesGetAll = channel.unary_unary(
                '/databases.UserDatabases/UserDatabasesGetAll',
                request_serializer=databases__pb2.IDs.SerializeToString,
                response_deserializer=databases__pb2.ListUserDatabasesInfo.FromString,
                )
        self.UserDatabasesNewName = channel.unary_unary(
                '/databases.UserDatabases/UserDatabasesNewName',
                request_serializer=databases__pb2.EditName.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )
        self.UserDatabasesNewDescription = channel.unary_unary(
                '/databases.UserDatabases/UserDatabasesNewDescription',
                request_serializer=databases__pb2.EditDescription.SerializeToString,
                response_deserializer=databases__pb2.Status.FromString,
                )


class UserDatabasesServicer(object):
    """Missing associated documentation comment in .proto file."""

    def UserDatabasesCreate(self, request, context):
        """Manage functions
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UserDatabasesDelete(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UserDatabasesGetOne(self, request, context):
        """GetInfo
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UserDatabasesGetAll(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UserDatabasesNewName(self, request, context):
        """Edit
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UserDatabasesNewDescription(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_UserDatabasesServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'UserDatabasesCreate': grpc.unary_unary_rpc_method_handler(
                    servicer.UserDatabasesCreate,
                    request_deserializer=databases__pb2.CreateUserDatabases.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
            'UserDatabasesDelete': grpc.unary_unary_rpc_method_handler(
                    servicer.UserDatabasesDelete,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
            'UserDatabasesGetOne': grpc.unary_unary_rpc_method_handler(
                    servicer.UserDatabasesGetOne,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.UserDatabasesInfo.SerializeToString,
            ),
            'UserDatabasesGetAll': grpc.unary_unary_rpc_method_handler(
                    servicer.UserDatabasesGetAll,
                    request_deserializer=databases__pb2.IDs.FromString,
                    response_serializer=databases__pb2.ListUserDatabasesInfo.SerializeToString,
            ),
            'UserDatabasesNewName': grpc.unary_unary_rpc_method_handler(
                    servicer.UserDatabasesNewName,
                    request_deserializer=databases__pb2.EditName.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
            'UserDatabasesNewDescription': grpc.unary_unary_rpc_method_handler(
                    servicer.UserDatabasesNewDescription,
                    request_deserializer=databases__pb2.EditDescription.FromString,
                    response_serializer=databases__pb2.Status.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'databases.UserDatabases', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class UserDatabases(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def UserDatabasesCreate(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.UserDatabases/UserDatabasesCreate',
            databases__pb2.CreateUserDatabases.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UserDatabasesDelete(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.UserDatabases/UserDatabasesDelete',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UserDatabasesGetOne(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.UserDatabases/UserDatabasesGetOne',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.UserDatabasesInfo.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UserDatabasesGetAll(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.UserDatabases/UserDatabasesGetAll',
            databases__pb2.IDs.SerializeToString,
            databases__pb2.ListUserDatabasesInfo.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UserDatabasesNewName(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.UserDatabases/UserDatabasesNewName',
            databases__pb2.EditName.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UserDatabasesNewDescription(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/databases.UserDatabases/UserDatabasesNewDescription',
            databases__pb2.EditDescription.SerializeToString,
            databases__pb2.Status.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
